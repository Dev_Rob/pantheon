/*
 * Copyright (C) 2012 Pantheon-WoW <http://pantheonwow.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"

struct Killstreak
{
	Killstreak() : kills(0), lastKill(0), buff(0) {}
	~Killstreak()
	{

	}

	uint32 kills;
	uint64 lastKill;
	uint32 buff;
};

typedef std::map<uint64, Killstreak*> KillstreakMap;
class world_killstreak : PlayerScript
{
public:
	world_killstreak() : PlayerScript("KillstreakPlayerScript") { }

	KillstreakMap::iterator GetPlayer(Player * player, bool add = true)
	{
		uint64 guid = player->GetGUID();
		KillstreakMap::iterator itr = m_Streak.find(guid);
		if (itr == m_Streak.end() && add)
		{
			m_Streak[guid] = new Killstreak();
			itr = m_Streak.find(guid);
		}

		return itr;
	}

	void OnPVPKill(Player* killer, Player* victim)
	{
		// Prevent no killer / victim
		if (killer == NULL || victim == NULL) return;

		// Prevent suicide
		if (killer->GetGUID() == victim->GetGUID()) return;

		// Killer Update
		KillstreakMap::iterator klr = GetPlayer(killer);
		if (klr != m_Streak.end())
		{
			// Prevent killing the same person over and over
			if (klr->second->lastKill != victim->GetGUID())
			{
				klr->second->lastKill = victim->GetGUID();
				klr->second->kills++;
				if (klr->second->kills >= 15)
				{
					char buff[2048];
					sprintf(buff, "%s is on a killing spree with %u kills!!", killer->GetName(), klr->second->kills);
					sWorld->SendServerMessage(SERVER_MSG_STRING, buff);
					killer->CastSpell(killer, 36300, true);
					klr->second->buff++;
				}
			}
		}

		// Victim Update
		klr = GetPlayer(victim);
		if (klr != m_Streak.end())
		{
			klr->second->kills = NULL;
			klr->second->lastKill = NULL;
			if (klr->second->buff != NULL)
			{
				char buff[2048];
				sprintf(buff, "%s has ended %s's killing spree!", killer->GetName(), victim->GetName());
				sWorld->SendServerMessage(SERVER_MSG_STRING, buff);
				klr->second->buff = NULL;
			}
		}
	}

	void OnLogout(Player* player)
	{
		// Crash fix
		if (player == NULL) return;

		KillstreakMap::iterator plr = GetPlayer(player, false);
		if (plr == m_Streak.end())
			return;

		m_Streak.erase(plr);
	}

private:
	KillstreakMap m_Streak;
};

void AddSC_worldpvp()
{
	new world_killstreak();
}

class npc_transmogrify : public CreatureScript
{
public:
	npc_transmogrify() : CreatureScript("npc_transmogrify") { }
	bool OnGossipHello(Player* pPlayer, Creature* pCreature)
	{
		pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Make exchange! (100 gold)", GOSSIP_SENDER_MAIN, 1);
		pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Make item clean.", GOSSIP_SENDER_MAIN, 2);
		pPlayer->SEND_GOSSIP_MENU(54442, pCreature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->SendCloseGossip();
		switch (action)
		{
		case 1:
			TransmogrifyItem(player, creature);
			break;
		case 2:
			ClearItem(player, creature);
			break;

		}
		return true;
	}

	void TransmogrifyItem(Player* player, Creature* creature)
	{
		ChatHandler handler(player);
		Item *trItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, INVENTORY_SLOT_ITEM_START);
		Item *displayItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, INVENTORY_SLOT_ITEM_START + 1);
		if (!trItem || !displayItem)
		{
			handler.PSendSysMessage("Put items in the first and second slot!");
			return;
		}

		if (player->GetMoney() < 100 * 100 * 100) // 100 gold
		{
			handler.PSendSysMessage("It costs %u gold!", 100);
			return;
		}

		TransmogrificationResult result = trItem->SetTransmogDisplay(displayItem->GetTemplate()->ItemId);
		switch(result)
		{
			case TRANSMOG_ERR: handler.PSendSysMessage("Unknown error occurred!"); return;
			case TRANSMOG_ERR_NOITEM: handler.PSendSysMessage("Item information missing"); return;
			case TRANSMOG_ERR_QUALITY: handler.PSendSysMessage("Item quality can not be legendary or common"); return;
			case TRANSMOG_ERR_CLASS: handler.PSendSysMessage("Item required classes must match"); return;
			case TRANSMOG_ERR_RACE: handler.PSendSysMessage("Item required race must match"); return;
			case TRANSMOG_ERR_TYPE: handler.PSendSysMessage("Item types must match (Armor -> Armor, Plate -> Plate, Weapon -> Weapon etc)"); return;
			case TRANSMOG_ERR_FISHING: handler.PSendSysMessage("Fishing rods can not be transmogrified."); return;
			case TRANSMOG_ERR_WEAPONTYPE: handler.PSendSysMessage("Item weapon types must match"); return;
		}
		WorldPacket data;
		data << uint8(INVENTORY_SLOT_BAG_0);
		data << uint8(trItem->GetSlot());
		player->GetSession()->HandleAutoEquipItemOpcode(data);

		player->ModifyMoney(-1 * 100 * 100 * 100); // 100 gold
		WorldPacket data1(SMSG_LOOT_MONEY_NOTIFY, 4 + 1);
		data1 << uint32(-1 * 100 * 100 * 100);
		data1 << uint8(1);   // "You loot..."
		player->GetSession()->SendPacket(&data1);
		creature->GetAI()->DoCast(63491);
	}

	void ClearItem(Player *player, Creature* creature)
	{
		ChatHandler handler(player);
		Item *trItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, INVENTORY_SLOT_ITEM_START);
		if (!trItem)
		{
			handler.PSendSysMessage("Put item in the first slot!");
			return;
		}

		trItem->RemoveTransmogDisplay();
		WorldPacket data;
		data << uint8(INVENTORY_SLOT_BAG_0);
		data << uint8(trItem->GetSlot());
		player->GetSession()->HandleAutoEquipItemOpcode(data);
		creature->GetAI()->DoCast(63491);
	}
};

class npc_sharedbank : public CreatureScript
{
public:
	npc_sharedbank() : CreatureScript("npc_sharedbank") { }

	bool OnGossipHello(Player* pPlayer, Creature* pCreature)
	{
	}
};

void AddSC_transmogrify_script()
{
	new npc_transmogrify;
}